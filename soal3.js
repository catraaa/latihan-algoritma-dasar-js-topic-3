/**
 * replace . with ()
 * use loop
 */

function solution(str) {
  var newStr = str.split('');

  for (var i = 0; i < newStr.length; i++) {
    if (newStr[i] === '.') {
      newStr[i] = '()';
    }
  }

  return newStr.join('');

  // *** using replace() method ***
  // let newStr = str.replace(/\./g, '()');
  // return newStr;
}

console.log(solution('1.1.1.1')); // expected 1()1()1()1
console.log(solution('192.168.1.1')); // expected 192()168()1()1
