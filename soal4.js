/**
 * sum all digits in a number
 */

function solution(n) {
  // cara 1
  // return ((n - 1) % 9) + 1;

  // cara 2
  // var sum = 0;

  // while (n) {
  //   sum += n % 10;
  //   n = Math.floor(n / 10);
  // }
  // return sum;

  // cara 3
  const str = n.toString();
  const splitStr = str.split('');
  console.log(splitStr);

  let result = 0;

  for (let i = 0; i < splitStr.length; i++) {
    let cvToNumber = parseInt(splitStr[i]);
    console.log(cvToNumber);
    result += cvToNumber;
  }
  return result;
}

console.log(solution(2022));
