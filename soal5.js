function solution(str) {
  if (str.includes('a' || 'b')) {
    let splitStr = str.split('');

    let indexA = splitStr.indexOf('a');
    // console.log(indexA);
    let indexB = splitStr.indexOf('b');
    // console.log(indexB);

    if ((indexB - indexA) > 3) {
      return "YES";
    } else {
      return "NO";
    }
  } else {
    return "String yang dimasukkan tidak terdapat karakter a dan b";
  }
}

console.log(solution("acdebae")); // expected output YES
console.log(solution("cdaecba")); // expected output NO
console.log(solution("jkhdfjei"));