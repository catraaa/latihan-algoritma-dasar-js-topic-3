// x = nominal uang yang akan ditarik
// y = saldo di rekening
// x harus kelipatan 5,
// y harus lebih besar / sama dengan y,
// setiap transaksi berhasil, terkena biaya admin 0.5
// return saldo setelah melakukan transaksi

function solution(x, y) {
  if (x % 5 == 0 && y >= x) {
    y = y - (x + 0.5);
  }
  return y;
}

console.log(solution(30, 120));
console.log(solution(42, 100));
